package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static myprojects.automation.assignment2.utils.Properties.getBaseAdminUrl;

public class LoginTest extends BaseScript {

    private static WebDriver driver;

    public static void main(String[] args)  {
        // TODO Script to execute login and logout steps
        driver = getConfiguredDriver(getDriver());
        executeLogin();
        executeLogout();
        printTitle(driver);
        driver.quit();
    }
     private static void  executeLogin(){
        // get url
        driver.get(getBaseAdminUrl());
        // find element
        WebElement emailElement = driver.findElement(By.id("email"));
        // send keys to element
        emailElement.sendKeys("webinar.test@gmail.com");

        WebElement passwdElement = driver.findElement(By.id("passwd"));
        passwdElement.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement submitLoginElement = driver.findElement(By.name("submitLogin"));
        submitLoginElement.click();
    }
    private static void executeLogout() {
        // find the element
         WebElement employee_infos_Element = driver.findElement(By.id("employee_infos"));
        employee_infos_Element.click();
        // click the element
        WebElement header_logout_Element = driver.findElement(By.id("header_logout"));
        header_logout_Element.click();
    }
}
