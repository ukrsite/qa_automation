package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseScript;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static myprojects.automation.assignment2.utils.Properties.getBaseAdminUrl;


public abstract class CheckMainMenuTest extends BaseScript {

    private static WebDriver driver;

    public static void main(String[] args) {
        // TODO Script to check Main Menu items
        driver = getConfiguredDriver(getDriver());
        executeLogin();
        checkMainMenuItems();
        printTitle(driver);
        driver.quit();
    }
    private static void executeLogin() {
        // get url
        driver.get(getBaseAdminUrl());
        // find element
        WebElement emailElement = driver.findElement(By.id("email"));
        // send keys to element
        emailElement.sendKeys("webinar.test@gmail.com");

        WebElement passwdElement = driver.findElement(By.id("passwd"));
        passwdElement.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement submitLoginElement = driver.findElement(By.name("submitLogin"));
        submitLoginElement.click();
    }
    private static void checkMainMenuItems() {
        By locator = By.className("maintab");
        List<WebElement> maintabElement =  driver.findElements(locator);
        // get main menu size
        int intItem = maintabElement.size();
        // get url of main menu
        String urlItem = driver.getCurrentUrl();
        // check menu items
        for(int i=0; i<intItem; i++) {
            // renew main menu list
            maintabElement =  driver.findElements(locator);
            // check next menu item
            maintabElement.get(i).click();
            // print page title
            System.out.println("The page title is : " + driver.getTitle());
            // get menu item url
            String urlItemsBeforeRefresh = driver.getCurrentUrl();
            driver.navigate().refresh();
            String urlItemsAfterRefresh = driver.getCurrentUrl();
            // check menu item url
            if (urlItemsAfterRefresh.equals(urlItemsBeforeRefresh)) {
                System.out.println("The menu item URL is not changed after refresh");
            } else {
                System.out.println("The menu item URL is changed after refresh");
            }
            // back to dashboard
            driver.get(urlItem);
        }
    }
}
