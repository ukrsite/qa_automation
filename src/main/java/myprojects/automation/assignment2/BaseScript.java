package myprojects.automation.assignment2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {

    /**
     *
     * @return New instance of {@link WebDriver} object.
     */
    protected static WebDriver getDriver() {
        // TODO return  WebDriver instance
        // set separatorPath value
        String separatorPath = System.getProperty("file.separator");
        // set propertyPath value
        String propertyPath = System.getProperty("user.dir") + separatorPath +  "drivers"
                + separatorPath + "chromedriver.exe";
        // specify webdriver.chrome.driver property
        System.setProperty("webdriver.chrome.driver", propertyPath);
        // return driver instance
        return new ChromeDriver();
    }
    protected static WebDriver getConfiguredDriver(WebDriver driver) {
        //Maximize Browser Window
        driver.manage().window().maximize();
        // set implicity waits
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver;
    }
    protected static void printTitle(WebDriver driver){
        System.out.println("Current windows title : " + driver.getTitle());
    }
}
